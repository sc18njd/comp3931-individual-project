#ifndef __VECTOR_H_
#define __VECTOR_H_
#define _USE_MATH_DEFINES

#include <cmath>
#include <math.h>
#include <cstring>
#include <GL/glu.h>
#include <GL/glut.h>

struct Vec3{
    GLfloat x,y,z;

    Vec3();
    Vec3(GLfloat x, GLfloat y, GLfloat z);
    static Vec3 normalize(const Vec3& vector);
    static Vec3 cross(const Vec3& a, const Vec3& b);
    static GLfloat dot(const Vec3& a, const Vec3& b);

	inline Vec3& operator=(const Vec3& rhs){
        this->x = rhs.x;
        this->y = rhs.y;
        this->z = rhs.z;
		return *this;
	}

};


extern inline Vec3 operator+(const Vec3& lhs, const Vec3& rhs){
    GLfloat x,y,z;

    x = lhs.x + rhs.x;
    y = lhs.y + rhs.y;
    z = lhs.z + rhs.z;

    return Vec3(x,y,z);
};

extern inline Vec3 operator-(const Vec3& lhs, const Vec3& rhs){
    GLfloat x,y,z;

    x = lhs.x - rhs.x;
    y = lhs.y - rhs.y;
    z = lhs.z - rhs.z;

    return Vec3(x,y,z);
};

extern inline Vec3 operator*(const float lhs, const Vec3&rhs){
    GLfloat x,y,z;
    
    x = lhs * rhs.x;
    y = lhs* rhs.y;
    z = lhs * rhs.z;

    return Vec3(x,y,z);
};

#endif