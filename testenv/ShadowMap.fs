#version 120

uniform sampler2D ShadowMap;

varying vec4 TexCoord;
varying vec3 Normal, LightDirection;


void main()
{
	//setting variable
	float LightDistance = sqrt(dot(LightDirection,LightDirection)); //calc light distance
	float shadow = max(dot(normalize(Normal), LightDirection / LightDistance), 0.0); //calculate lighting effect on current frag
	vec3 ambient = 	gl_LightSource[0].ambient.rgb;
	vec3 diffuse = gl_LightSource[0].diffuse.rgb;
	
	//perform perspective divide
	vec3 TexCoordProj = TexCoord.xyz / TexCoord.w;
		
	//check if values in range 0 to 1
	if(TexCoordProj.x >= 0.0 && TexCoordProj.x < 1.0 &&
	   TexCoordProj.y >= 0.0 && TexCoordProj.y < 1.0 &&
	   TexCoordProj.z >= 0.0 && TexCoordProj.z < 1.0)
	{
		float closestDepth = texture2D(ShadowMap, TexCoordProj.xy).r;
		//check if current fragment is in shadow
		if(TexCoordProj.z > closestDepth)
		{
			shadow = 0.0;
		}
	}
	
	vec3 lighting = gl_Color.rgb * ( ambient.rgb + diffuse.rgb * shadow) ;
	
	
	gl_FragColor.rgb = lighting.rgb; //set rgb values for the program
}