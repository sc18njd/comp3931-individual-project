#ifndef __SHADER_H
#define __SHADER_H

#include <GL/glew.h>
#include <QGLWidget>
#include <GL/glu.h>
#include <GL/glut.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include "Matrix.h"
#include "Vector.h"

class ShaderProgram
	{
  protected:
    GLuint VertexShader, FragmentShader, Program;

    GLuint LoadShader(char *FileName, GLenum Type);
    void SetDefault();
		
    public:
    ShaderProgram();
    ~ShaderProgram();

    GLuint getProgram();

    bool Load(char *VertexFilename, char *FragmentFilename);
    void Destroy();
    void setFloat(const std::string &name, float value);
    void setInt(const std::string &name, float value);
    void setMat4(const std::string &name, const Mat4 &mat);
    void setVec3(const std::string &name, const Vec3 &vec);
    void use();
    void disable();
	};

#endif
