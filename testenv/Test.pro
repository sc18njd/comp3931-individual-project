
TEMPLATE = app
FORMS = dialog.ui
TARGET = Test
INCLUDEPATH += . /opt/local/include

QT += widgets opengl gui 

LIBS += -lGLU -lglut -lGLEW

# Input
HEADERS += ShaderProgram.h TestWidget.h TestWindow.h Matrix.h Vector.h Camera.h
SOURCES += TestMain.cpp \
           TestWidget.cpp \
           TestWindow.cpp \
	       ShaderProgram.cpp \
           Matrix.cpp \
           Vector.cpp \
           Camera.cpp
       
