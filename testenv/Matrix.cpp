#include "Matrix.h"
/*
*   class for Matrices, my own implementation of required matrix functionality from similar glm library
*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//4x4 Matrix functions

//constructor class, sets matrix to identity matrix
Mat4::Mat4(){
    memset(&this->matrixElements,0, sizeof(this->matrixElements));
    this->matrixElements[0] = 1;
    this->matrixElements[5] = 1;
    this->matrixElements[10] = 1;
    this->matrixElements[15] = 1;
}

//constructor class with parameters, sets matrix to passed values
Mat4::Mat4(GLfloat elements[]){
    memset(&this->matrixElements, 0, sizeof(this->matrixElements));
    for (int i=0; i<16; i++){
        this->matrixElements[i] = elements[i];
    }
}

//Function to create perspective matrix
Mat4 Mat4::createPerspectiveMatrix(GLfloat fov, GLfloat aspect, GLfloat nearPlane, GLfloat farPlane){
    Mat4 tempMatrix = Mat4();
    tempMatrix.matrixElements[0] = (1.0f/ std::tan((fov * M_PI / 180.0f) / 2.0f)) / aspect;
    tempMatrix.matrixElements[5] = 1.0f / std::tan((fov * M_PI / 180.0f) / 2.0f);
    tempMatrix.matrixElements[10] = (farPlane + nearPlane) / (nearPlane - farPlane);
    tempMatrix.matrixElements[11] = -1.0f;
    tempMatrix.matrixElements[14] = (2.0f * nearPlane * farPlane) / (nearPlane - farPlane);
    tempMatrix.matrixElements[15] = 0.0f;

    return tempMatrix;
}

//Function for creating view matrix
Mat4 Mat4::createViewMatrix(const Vec3& eye, const Vec3& centre, const Vec3& up){
    Vec3 Z = Vec3::normalize(eye-centre);//forward
    Vec3 X = Vec3::normalize(Vec3::cross(up,Z));//side
    Vec3 Y = Vec3::normalize(Vec3::cross(Z,X));//up

    Mat4 tempMatrix = Mat4();
	tempMatrix.matrixElements[0] = X.x;
	tempMatrix.matrixElements[1] = Y.x;
	tempMatrix.matrixElements[2] = Z.x;
	tempMatrix.matrixElements[4] = X.y;
	tempMatrix.matrixElements[5] = Y.y;
	tempMatrix.matrixElements[6] = Z.y;
	tempMatrix.matrixElements[8] = X.z;
	tempMatrix.matrixElements[9] = Y.z;
	tempMatrix.matrixElements[10] = Z.z;
	tempMatrix.matrixElements[12] = -Vec3::dot(X, eye);
	tempMatrix.matrixElements[13] = -Vec3::dot(Y, eye);
	tempMatrix.matrixElements[14] = -Vec3::dot(Z, eye);

    return tempMatrix;
}


//function for creating orthographic matrix
Mat4 Mat4::createOrthographicMatrix(GLfloat bottom, GLfloat top, GLfloat left, GLfloat right, GLfloat nearPlane, GLfloat farPlane){
    Mat4 tempMatrix = Mat4();

    tempMatrix.matrixElements[0] = 2.0f / (right - left);
    tempMatrix.matrixElements[5] = 2.0f / (top - bottom);
    tempMatrix.matrixElements[10] = -2.0f / (farPlane - nearPlane);
    tempMatrix.matrixElements[12] = -(right + left) / (right - left);
    tempMatrix.matrixElements[13] = -(top + bottom) / (top - bottom);
    tempMatrix.matrixElements[14] = -(farPlane + nearPlane) / (farPlane - nearPlane);

    return tempMatrix;
}


//function to calculate determinant of a 2x2 matrix
float getDet2x2(const float *elements, int index0, int index1, int index2, int index3){
   return elements[index0] * elements[index3] - elements[index2] * elements[index1]; 
}


//function to calculate determinant of a 3x3 matrix
float getDet3x3(const float *elements, int index0, int index1, int index2, int index3, int index4, int index5, int index6, int index7, int index8){
	float det = 0.0f;

	det += elements[index0] * getDet2x2(elements, index4, index5, index7, index8);
	det -= elements[index3] * getDet2x2(elements, index1, index2, index7, index8);
	det += elements[index6] * getDet2x2(elements, index1, index2, index4, index5);

	return det;
}

//function to calculate inverse 4x4 matrix
Mat4 Mat4::inverseMatrix(const Mat4 &m){
    const float *elements = m.matrixElements;

    float det = 0.0f;

    //calulate determinant by reducing problem into smaller sizes and finding determinants of smaller sized matrices from the 4x4 values
	det += elements[0] * getDet3x3(elements, 5, 6, 7, 9, 10, 11, 13, 14, 15);
	det -= elements[4] * getDet3x3(elements, 1, 2, 3, 9, 10, 11, 13, 14, 15);
	det += elements[8] * getDet3x3(elements, 1, 2, 3, 5, 6, 7, 13, 14, 15);
	det -= elements[12] * getDet3x3(elements, 1, 2, 3, 5, 6, 7, 9, 10, 11);

    Mat4 tempMatrix = Mat4();

    //use determinant to calculate inverse values
    tempMatrix.matrixElements[0] = getDet3x3(elements, 5, 6, 7, 9, 10, 11, 13, 14, 15) / det;
	tempMatrix.matrixElements[1] = -getDet3x3(elements, 1, 2, 3, 9, 10, 11, 13, 14, 15) / det;
	tempMatrix.matrixElements[2] = getDet3x3(elements, 1, 2, 3, 5, 6, 7, 13, 14, 15) / det;
	tempMatrix.matrixElements[3] = -getDet3x3(elements, 1, 2, 3, 5, 6, 7, 9, 10, 11) / det;
	tempMatrix.matrixElements[4] = -getDet3x3(elements, 4, 6, 7, 8, 10, 11, 12, 14, 15) / det;
	tempMatrix.matrixElements[5] = getDet3x3(elements, 0, 2, 3, 8, 10, 11, 12, 14, 15) / det;
	tempMatrix.matrixElements[6] = -getDet3x3(elements, 0, 2, 3, 4, 6, 7, 12, 14, 15) / det;
	tempMatrix.matrixElements[7] = getDet3x3(elements, 0, 2, 3, 4, 6, 7, 8, 10, 11) / det;
	tempMatrix.matrixElements[8] = getDet3x3(elements, 4, 5, 7, 8, 9, 11, 12, 13, 15) / det;
	tempMatrix.matrixElements[9] = -getDet3x3(elements, 0, 1, 3, 8, 9, 11, 12, 13, 15) / det;
	tempMatrix.matrixElements[10] = getDet3x3(elements, 0, 1, 3, 4, 5, 7, 12, 13, 15) / det;
	tempMatrix.matrixElements[11] = -getDet3x3(elements, 0, 1, 3, 4, 5, 7, 8, 9, 11) / det;
	tempMatrix.matrixElements[12] = -getDet3x3(elements, 4, 5, 6, 8, 9, 10, 12, 13, 14) / det;
	tempMatrix.matrixElements[13] = getDet3x3(elements, 0, 1, 2, 8, 9, 10, 12, 13, 14) / det;
	tempMatrix.matrixElements[14] = -getDet3x3(elements, 0, 1, 2, 4, 5, 6, 12, 13, 14) / det;
	tempMatrix.matrixElements[15] = getDet3x3(elements, 0, 1, 2, 4, 5, 6, 8, 9, 10) / det;

    return tempMatrix;
}


//Function to create a rotation matrix 
Mat4 Mat4::rotateMatrix(const Mat4& m, GLfloat angle, const Vec3& vector){
    float cosA = cos(angle);
    float sinA = sin(angle);

    Vec3 axis = Vec3::normalize(vector);
    Mat4 Result;

    Result.matrixElements[0] = cosA + (1-cosA) * axis.x * axis.x;
    Result.matrixElements[1] = (1-cosA) * axis.x * axis.y + sinA * axis.z;
    Result.matrixElements[2] = (1-cosA) * axis.x * axis.z - sinA * axis.y;
    Result.matrixElements[3] = 0;

    Result.matrixElements[4] = (1-cosA) * axis.y *axis.x - sinA * axis.z;
    Result.matrixElements[5] = cosA + (1-cosA) * axis.y * axis.y;
    Result.matrixElements[6] = (1-cosA) * axis.y * axis.z + sinA * axis.x;
    Result.matrixElements[7] = 0;

    Result.matrixElements[8] = (1 - cosA) * axis.z * axis.x + sinA * axis.y;
    Result.matrixElements[9] = (1 - cosA) * axis.z * axis.y - sinA * axis.x;
    Result.matrixElements[10] = cosA + (1 - cosA) * axis.z * axis.z;
    Result.matrixElements[11] = 0;

    Result.matrixElements[12] = 0;
    Result.matrixElements[13] = 0;
    Result.matrixElements[14] = 0;
    Result.matrixElements[15] = 1;

    return m*Result;
}

//-----------------------------------------------------------------------------------
// 3x3 Matrix functions

//constructor class which makes identity matrix
Mat3::Mat3(){
    memset(&this->matrixElements,0, sizeof(this->matrixElements));
    this->matrixElements[0] = 1;
    this->matrixElements[4] = 1;
    this->matrixElements[8] = 1;
}

////constructor which uses parameters to set matrix
Mat3::Mat3(const Mat4 m){
    memset(&this->matrixElements,0, sizeof(this->matrixElements));

    this->matrixElements[0] = m.matrixElements[0];
    this->matrixElements[1] = m.matrixElements[1];
    this->matrixElements[2] = m.matrixElements[2];

    this->matrixElements[3] = m.matrixElements[4];
    this->matrixElements[4] = m.matrixElements[5];
    this->matrixElements[5] = m.matrixElements[6];

    this->matrixElements[6] = m.matrixElements[8];
    this->matrixElements[7] = m.matrixElements[9];
    this->matrixElements[8] = m.matrixElements[10];
}
