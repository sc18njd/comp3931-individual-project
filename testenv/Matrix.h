#ifndef __MATRIX_H_
#define __MATRIX_H_
#define _USE_MATH_DEFINES

#include <cmath>
#include <math.h>
#include <cstring>
#include <GL/glu.h>
#include <GL/glut.h>
#include "Vector.h"

struct Mat4{
	GLfloat matrixElements[16];

    Mat4();
    Mat4(GLfloat elements[]);
	static Mat4 createPerspectiveMatrix(GLfloat fov, GLfloat aspect, GLfloat nearPlane, GLfloat farPlane);
	static Mat4 createViewMatrix(const Vec3& eye, const Vec3& centre, const Vec3& up);
	static Mat4 createOrthographicMatrix(GLfloat left, GLfloat right, GLfloat top, GLfloat bottom, GLfloat nearPlane, GLfloat farPlane);
	static Mat4 inverseMatrix(const Mat4& matrix);
    static Mat4 rotateMatrix(const Mat4& m,GLfloat angle, const Vec3& vector);

    /*
    *   operators for the 4x4 matrix class
    */
	inline Mat4& operator=(const Mat4& rhs){
		for(int i=0; i<16; i++){
			this->matrixElements[i] = rhs.matrixElements[i];	
		}
		return *this;
	}
	inline Mat4& operator+=(const Mat4& rhs){
		for(int i=0; i<16; i++){
			this->matrixElements[i] += rhs.matrixElements[i];
		}
		return *this;
	}
	inline Mat4& operator -=(const Mat4& rhs){
		for(int i=0; i<16; i++){
			this->matrixElements[i] -= rhs.matrixElements[i];
		}
		return *this;
	}
	inline Mat4& operator*=(const GLfloat& rhs){
		for(int i = 0; i<16; i++){
			this->matrixElements[i] *= rhs;
		}
		return *this;
	}

    //matrix multiplication
	inline Mat4& operator*=(const Mat4& rhs){
   
    Mat4 tempMatrix = Mat4();

    // assign values to temporary floats
    tempMatrix.matrixElements[0] = this->matrixElements[0] * rhs.matrixElements[0] + this->matrixElements[1] * rhs.matrixElements[4] + this->matrixElements[2] * rhs.matrixElements[8] + this->matrixElements[3] * rhs.matrixElements[12];
    tempMatrix.matrixElements[1] = this->matrixElements[0] * rhs.matrixElements[1] + this->matrixElements[1] * rhs.matrixElements[5] + this->matrixElements[2] * rhs.matrixElements[9] + this->matrixElements[3] * rhs.matrixElements[13];
    tempMatrix.matrixElements[2] = this->matrixElements[0] * rhs.matrixElements[2] + this->matrixElements[1] * rhs.matrixElements[6] + this->matrixElements[2] * rhs.matrixElements[10] + this->matrixElements[3] * rhs.matrixElements[14];
    tempMatrix.matrixElements[3] = this->matrixElements[0] * rhs.matrixElements[3] + this->matrixElements[1] * rhs.matrixElements[7] + this->matrixElements[2] * rhs.matrixElements[11] + this->matrixElements[3] * rhs.matrixElements[15];

    tempMatrix.matrixElements[4] = this->matrixElements[4] * rhs.matrixElements[0] + this->matrixElements[5] * rhs.matrixElements[4] + this->matrixElements[6] * rhs.matrixElements[8] + this->matrixElements[7] * rhs.matrixElements[12];
    tempMatrix.matrixElements[5] = this->matrixElements[4] * rhs.matrixElements[1] + this->matrixElements[5] * rhs.matrixElements[5] + this->matrixElements[6] * rhs.matrixElements[9] + this->matrixElements[7] * rhs.matrixElements[13];
    tempMatrix.matrixElements[6] = this->matrixElements[4] * rhs.matrixElements[2] + this->matrixElements[5] * rhs.matrixElements[6] + this->matrixElements[6] * rhs.matrixElements[10] + this->matrixElements[7] * rhs.matrixElements[14];
    tempMatrix.matrixElements[7] = this->matrixElements[4] * rhs.matrixElements[3] + this->matrixElements[5] * rhs.matrixElements[7] + this->matrixElements[6] * rhs.matrixElements[11] + this->matrixElements[7] * rhs.matrixElements[15];

    tempMatrix.matrixElements[8] = this->matrixElements[8] * rhs.matrixElements[0] + this->matrixElements[9] * rhs.matrixElements[4] + this->matrixElements[10] * rhs.matrixElements[8] + this->matrixElements[11] * rhs.matrixElements[12];
    tempMatrix.matrixElements[9] = this->matrixElements[8] * rhs.matrixElements[1] + this->matrixElements[9] * rhs.matrixElements[5] + this->matrixElements[10] * rhs.matrixElements[9] + this->matrixElements[11] * rhs.matrixElements[13];
    tempMatrix.matrixElements[10] = this->matrixElements[8] * rhs.matrixElements[2] + this->matrixElements[9] * rhs.matrixElements[6] + this->matrixElements[10] * rhs.matrixElements[10] + this->matrixElements[11] * rhs.matrixElements[14];
    tempMatrix.matrixElements[11] = this->matrixElements[8] * rhs.matrixElements[3] + this->matrixElements[9] * rhs.matrixElements[7] + this->matrixElements[10] * rhs.matrixElements[11] + this->matrixElements[11] * rhs.matrixElements[15];

    tempMatrix.matrixElements[12] = this->matrixElements[12] * rhs.matrixElements[0] + this->matrixElements[13] * rhs.matrixElements[4] + this->matrixElements[14] * rhs.matrixElements[8] + this->matrixElements[15] * rhs.matrixElements[12];
    tempMatrix.matrixElements[13] = this->matrixElements[12] * rhs.matrixElements[1] + this->matrixElements[13] * rhs.matrixElements[5] + this->matrixElements[14] * rhs.matrixElements[9] + this->matrixElements[15] * rhs.matrixElements[13];
    tempMatrix.matrixElements[14] = this->matrixElements[12] * rhs.matrixElements[2] + this->matrixElements[13] * rhs.matrixElements[6] + this->matrixElements[14] * rhs.matrixElements[10] + this->matrixElements[15] * rhs.matrixElements[14];
    tempMatrix.matrixElements[15] = this->matrixElements[12] * rhs.matrixElements[3] + this->matrixElements[13] * rhs.matrixElements[7] + this->matrixElements[14] * rhs.matrixElements[11] + this->matrixElements[15] * rhs.matrixElements[15];

    //assign values to current matrix

    this->matrixElements[0] =  tempMatrix.matrixElements[0];
    this->matrixElements[1] =  tempMatrix.matrixElements[1];
    this->matrixElements[2] =  tempMatrix.matrixElements[2];
    this->matrixElements[3] =  tempMatrix.matrixElements[3];

    this->matrixElements[4] =  tempMatrix.matrixElements[4];
    this->matrixElements[5] =  tempMatrix.matrixElements[5];
    this->matrixElements[6] =  tempMatrix.matrixElements[6];
    this->matrixElements[7] =  tempMatrix.matrixElements[7];

	this->matrixElements[8] =  tempMatrix.matrixElements[8];
    this->matrixElements[9] =  tempMatrix.matrixElements[9];    
    this->matrixElements[10] = tempMatrix.matrixElements[10];
    this->matrixElements[11] = tempMatrix.matrixElements[11];

    this->matrixElements[12] = tempMatrix.matrixElements[12];
    this->matrixElements[13] = tempMatrix.matrixElements[13];
    this->matrixElements[14] = tempMatrix.matrixElements[14];
    this->matrixElements[15] = tempMatrix.matrixElements[15];

    return  *this;
	}

};

extern inline Mat4 operator+(const Mat4& lhs, const Mat4& rhs){
    Mat4 tempMatrix = Mat4(lhs);
    tempMatrix += rhs;
    return tempMatrix;
};

extern inline Mat4 operator-(const Mat4& lhs, const Mat4& rhs){
    Mat4 tempMatrix = Mat4(lhs);
    tempMatrix -= rhs;
    return tempMatrix;
};

extern inline Mat4 operator*(const Mat4& lhs, const Mat4& rhs){
    Mat4 tempMatrix = Mat4(lhs);
    tempMatrix *= rhs;
    return tempMatrix;
};

extern inline Mat4 operator-(const Mat4& lhs, const GLfloat& rhs){
    Mat4 tempMatrix = Mat4(lhs);
    tempMatrix *= rhs;
    return tempMatrix;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////

struct Mat3{
    GLfloat matrixElements[9];

    Mat3();
    Mat3(const Mat4 m);

    //required 3x3 matrix related operators

    inline Mat3& operator=(const Mat3& rhs){
	for(int i=0; i<9; i++){
		this->matrixElements[i] = rhs.matrixElements[i];	
	}
	return *this;
	}

};

//3x3 matrix multiplied by 3x1 vector
extern inline Vec3 operator*(const Mat3& lhs, const Vec3& rhs){

    float temp_0 =lhs.matrixElements[0] * rhs.x + lhs.matrixElements[1] * rhs.y + lhs.matrixElements[2] * rhs.z;
    float temp_1 =lhs.matrixElements[3] * rhs.x + lhs.matrixElements[4] * rhs.y + lhs.matrixElements[5] * rhs.z;
    float temp_2 =lhs.matrixElements[6] * rhs.x + lhs.matrixElements[7] * rhs.y + lhs.matrixElements[8] * rhs.z;

    return Vec3(temp_0, temp_1, temp_2);
};

#endif