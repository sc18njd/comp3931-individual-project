/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QDialogButtonBox *buttonBox;
    QTextEdit *posX;
    QTextEdit *posY;
    QTextEdit *posZ;
    QLabel *label1;
    QLabel *label2;
    QLabel *label3;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QString::fromUtf8("Dialog"));
        Dialog->resize(696, 204);
        buttonBox = new QDialogButtonBox(Dialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(590, 70, 81, 241));
        buttonBox->setOrientation(Qt::Vertical);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        posX = new QTextEdit(Dialog);
        posX->setObjectName(QString::fromUtf8("posX"));
        posX->setGeometry(QRect(50, 80, 121, 21));
        posY = new QTextEdit(Dialog);
        posY->setObjectName(QString::fromUtf8("posY"));
        posY->setGeometry(QRect(200, 80, 121, 21));
        posZ = new QTextEdit(Dialog);
        posZ->setObjectName(QString::fromUtf8("posZ"));
        posZ->setGeometry(QRect(360, 80, 111, 21));
        label1 = new QLabel(Dialog);
        label1->setObjectName(QString::fromUtf8("label1"));
        label1->setGeometry(QRect(50, 60, 71, 16));
        label2 = new QLabel(Dialog);
        label2->setObjectName(QString::fromUtf8("label2"));
        label2->setGeometry(QRect(370, 60, 58, 16));
        label3 = new QLabel(Dialog);
        label3->setObjectName(QString::fromUtf8("label3"));
        label3->setGeometry(QRect(210, 60, 58, 16));

        retranslateUi(Dialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), Dialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Dialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "Dialog"));
        label1->setText(QApplication::translate("Dialog", "Pos X"));
        label2->setText(QApplication::translate("Dialog", "Pos Z"));
        label3->setText(QApplication::translate("Dialog", "Pos Y"));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
