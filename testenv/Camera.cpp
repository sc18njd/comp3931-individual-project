#include "Camera.h"
/*
    Class to control the "camera" view to allow for user interaction and move the camera around the world
*/


const float MOVEMENT_SPEED = 0.5f;

//constructor class
Camera::Camera(){
    //assign NULL as world hasn't been initalised to view yet
    viewMatrix = NULL;
    viewMatrixInverse = NULL;

    position = Vec3(0.0f, 0.0f, 5.0f);
    viewDirection = Vec3(0.0f,0.0f,-1.0f);
    Up = Vec3(0.0f,1.0f,0.0f);
    right = Vec3(1.0f,0.0f,0.0f);

    strafeDirection = Vec3::cross(viewDirection, Up);

}

//destructor class
Camera::~Camera(){

}


//Function to set pointer to view and inverse view matrices
void Camera::setMatrixPointer(float* viewMatrix, float * viewMatrixInverse){
    this->viewMatrix = (Mat4*)viewMatrix;
    this->viewMatrixInverse = (Mat4*)viewMatrixInverse;

    calculateViewMatrix();
}

//Function to calculate the value of the view and inverse view matrix
void Camera::calculateViewMatrix(){
    if(viewMatrix != NULL){
        *viewMatrix = Mat4::createViewMatrix(position, position+viewDirection, Up);
        if(viewMatrixInverse != NULL){
            *viewMatrixInverse = Mat4::inverseMatrix(*viewMatrix);
        }
    }
}

//function to take in mouse inputs and rotate the camera accordingly
void Camera::mouseUpdate(const float newMousePositionX, const float newMousePositionY){

    //set the delta in the change from previous to new positions for mouse
    float deltaX = newMousePositionX - oldMousePositionX;
    float deltaY = newMousePositionY - oldMousePositionY;

    //check that the change in mouse movement isn't too large due to mouse moving off screen
    if(sqrt(((deltaX*deltaX)+(deltaY*deltaY)))>50.0f){
        //updates old mouse position and returns if change too large so as not to create wild movement
        oldMousePositionX = newMousePositionX;
        oldMousePositionY = newMousePositionY;
        return;
    }

    const float ROTATIONAL_SPEED = 0.01f;

    //update strafe matrix
    strafeDirection = Vec3::cross(viewDirection,Up);
    //set rotation matrix
    Mat4 rotator = Mat4::rotateMatrix(Mat4(),deltaX * ROTATIONAL_SPEED, Up) * Mat4::rotateMatrix(Mat4(),deltaY *ROTATIONAL_SPEED, strafeDirection);

    //set view direction vector
    viewDirection = Mat3(rotator) * viewDirection;

    //update saved mouse position
    oldMousePositionX = newMousePositionX;
    oldMousePositionY = newMousePositionY;

    //update view and inverse view matrices
    calculateViewMatrix();
}


/*
*    Set of functions to move the camera up, down, forward, backwards, right and left in world space
*    functions update the corresponding matrices and then update view and inverse view matrices using 
*    calculateViewMatrix() function
*/

void Camera::moveForward(){
    position = position + MOVEMENT_SPEED * viewDirection;
    calculateViewMatrix();
}

void Camera::moveBackward(){
    position = position - MOVEMENT_SPEED * viewDirection;
    calculateViewMatrix();
}

void Camera::moveRight(){   
    position = position + MOVEMENT_SPEED * strafeDirection;
    calculateViewMatrix();
}
    
void Camera::moveLeft(){
    position = position - MOVEMENT_SPEED * strafeDirection;
    calculateViewMatrix();
}

void Camera::moveUp(){
    position = position + MOVEMENT_SPEED * Up;
    calculateViewMatrix();
}

void Camera::moveDown(){
    position = position - MOVEMENT_SPEED * Up;
    calculateViewMatrix();
}