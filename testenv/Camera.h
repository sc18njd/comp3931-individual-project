#ifndef __CAMERA_H
#define __CAMERA_H

#include <iostream>
#include "Matrix.h"

class Camera{

    protected:
    Mat4 *viewMatrix, *viewMatrixInverse;

    public:
    Camera();
    ~Camera();

    void setMatrixPointer(float* viewMatrix, float * viewInverseMatirx = NULL);
    void look(const Vec3 &position, const Vec3& reference);
    void mouseUpdate(const float newMousePositionX, const float newMousePositionY);
    void moveForward();
    void moveBackward();
    void moveRight();
    void moveLeft();
    void moveUp();
    void moveDown();


    Vec3 position, reference, viewDirection, Up, right, strafeDirection;
    GLfloat oldMousePositionX, oldMousePositionY;

    private:
    void calculateViewMatrix();
};

#endif