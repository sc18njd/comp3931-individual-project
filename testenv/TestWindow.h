#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include "TestWidget.h"
#include <QGLWidget>
#include <QSlider>
#include <QLabel>
#include <QSpinBox>
#include <QTimer>
#include <QGridLayout>
#include <QPushButton>

class TestWindow: public QWidget
	{
	protected:
	void keyPressEvent(QKeyEvent *event);

	public:

	// constructor / destructor
	TestWindow(QWidget *parent);
	~TestWindow();

	// window layout
	QGridLayout *windowLayout;


	// beneath that, the main widget
	TestWidget *cubeWidget;

	QTimer *fpsTimer;
	QTimer *timer;


	// resets all the interface elements
	void ResetInterface();
	};

#endif
