#version 120

uniform mat4x4 ShadowMatrix;

varying vec4 TexCoord;
varying vec3 Normal, LightDirection;


void main()
{
	//setting varying variables to pass to fragment shader
	TexCoord = ShadowMatrix * gl_ModelViewMatrix * gl_Vertex;
	Normal = gl_NormalMatrix * gl_Normal;
	LightDirection = gl_LightSource[0].position.xyz - vec3(gl_ModelViewMatrix * gl_Vertex).xyz;

	//setting gl variables required
	gl_FrontColor = gl_Color;
	gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
}