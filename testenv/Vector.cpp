#include "Vector.h"

Vec3::Vec3(){
    this->x = 0;
    this->y = 0;
    this->z = 0;
}

Vec3::Vec3(GLfloat x, GLfloat y, GLfloat z){
    this->x = x;
    this->y = y;
    this->z = z;
}

Vec3 Vec3::normalize(const Vec3 &vector){
    float vectorLength = sqrt((vector.x * vector.x) + (vector.y * vector.y) + (vector.z * vector.z));

    return Vec3(vector.x / vectorLength, vector.y / vectorLength, vector.z / vectorLength);

}

Vec3 Vec3::cross(const Vec3& a, const Vec3& b){
    GLfloat x,y,z;
    x = a.y * b.z - a.z * b.y;
    y = a.z * b.x - a.x * b.z;
    z = a.x * b.y - a.y * b.x;

    return Vec3(x,y,z);
}

GLfloat Vec3::dot(const Vec3& a, const Vec3& b){
    GLfloat product = a.x * b.x + a.y * b.y + a.z * b.z; 
    return product;
}


