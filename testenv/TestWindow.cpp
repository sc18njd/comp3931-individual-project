#include "TestWindow.h"

//constructor/destructor
TestWindow::TestWindow(QWidget *parent) : QWidget(parent){
  //constructor
  //creation of layouts, Window layout is base layout
  windowLayout = new QGridLayout(this);


  // create main widget and add to main layout
  cubeWidget = new TestWidget(this);

  //create fps timer
  fpsTimer = new QTimer();

  connect(fpsTimer, SIGNAL(timeout()), cubeWidget, SLOT(showFPS()));
  fpsTimer->start(1000);

  this->timer = new QTimer();
  connect(timer, SIGNAL(timeout()),cubeWidget,SLOT(processing()));
  timer->start( 10);

  windowLayout->addWidget(cubeWidget,0,0,9,9);

  setLayout(windowLayout);
}

//function to recognise keys being pressed in the program
void TestWindow::keyPressEvent(QKeyEvent *event){
  //calls function from inside the cubeWidget code that deals with such events
  cubeWidget->keyPressed(event);
}

TestWindow::~TestWindow(){
  //destructor
  //force refresh
  cubeWidget->update();
  update();
}
