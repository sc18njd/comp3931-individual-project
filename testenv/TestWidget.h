#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <fstream>
#include <sstream>
#include <iostream>
#include <QKeyEvent>
#include <QMouseEvent>
#include "ShaderProgram.h"
#include "Matrix.h"
#include "Camera.h"
#include "ui_dialog.h"

struct materialStruct;

class TestWidget: public QGLWidget
	{ 

	Q_OBJECT

	public slots:
	void showFPS();
	void processing();

	public:
	TestWidget(QWidget *parent);

	void keyPressed(QKeyEvent * event);

	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget needs painting
	void paintGL();
	void resizeGL(int w, int h);
	void initDepthMap();
	void renderShadowMap();
	void initVBOandVAOs();
	void renderDepthToQuad();
	void renderScene();
	

	ShaderProgram shadowMap;
	Camera camera;

	private:
	void mouseDoubleClickEvent(QMouseEvent * event);
	void mouseMoveEvent(QMouseEvent * e);
	void loadDialog(const Ui_Dialog& );
	void unloadDialog(const Ui_Dialog&);

	struct LightingPar{	
		float x;
		float y;
		float z;
	};

	LightingPar _lightingPar;
	Ui_Dialog _ui;

	GLuint depthMap, depthMapFBO;
	GLenum initializeGLEW;
	Mat4 lightViewMatrix, lightProjectionMatrix;
	Mat4 viewMatrix, projectionMatrix, viewMatrixInverse, projectionMatrixInverse, biasMatrix, shadowMatrix;
	Vec3 lightPosition;
	bool showShadowMap, mouseTrack;
	int fps;

	}; // class TestWidget

#endif
