#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "ShaderProgram.h"

/*
*   Class to read in shader files and convert them to a program to use
*/

//constructor
ShaderProgram::ShaderProgram(){
  SetDefault();
}

//destructor
ShaderProgram::~ShaderProgram(){

}

//getter method for Program value
GLuint ShaderProgram::getProgram(){return Program;}

//function to load files with passed file names, returns true if xompiled correctly, false otherwise
bool ShaderProgram::Load(char *vertexFilename, char *fragmentFilename){
  
  bool error = false;
  //clear possible saved shaders
  Destroy();

  //loading the shaders into Program
  error |= ((VertexShader = LoadShader(vertexFilename, GL_VERTEX_SHADER)) == 0);
  error |= ((FragmentShader = LoadShader(fragmentFilename, GL_FRAGMENT_SHADER)) == 0);

  if(error){
    Destroy();
    return false;
  }

  //creating shader program and attaching shaders
  Program = glCreateProgram();
  glAttachShader(Program, VertexShader);
  glAttachShader(Program, FragmentShader);
  glLinkProgram(Program);

  //checking link status
  int linkStatus;
  glGetProgramiv(Program, GL_LINK_STATUS, &linkStatus);

  if(linkStatus == GL_FALSE){
    Destroy();
    return false;
  }

  return true;
}

//function to free memory 
void ShaderProgram::Destroy(){
  glDetachShader(Program, VertexShader);
  glDetachShader(Program, FragmentShader);

  glDeleteShader(VertexShader);
  glDeleteShader(FragmentShader);

  glDeleteProgram(Program);

  SetDefault();
}

//function to read in specific shader file and compile it, returns the shader programs value
GLuint ShaderProgram::LoadShader(char *filename, GLenum Type){
  std::string tempShaderCode;
  std::ifstream shaderFile;

  //ensuring ifstream objects can throw exceptions
  shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

  try{
    //open files
    shaderFile.open(filename);

    std::stringstream shaderStream;

    //read file
    shaderStream << shaderFile.rdbuf();
    //close file
    shaderFile.close();
    //convert to string
    tempShaderCode = shaderStream.str();
  }
  catch(std::ifstream::failure& e){
    std::cout << "Error :: SHADER :: FILE_NOT_SUCCESSFULLY_READ" << std::endl;
    return 0;
  }
  //convert to string
  const char * shaderCode = tempShaderCode.c_str();
  //create shader
  GLuint shader = glCreateShader(Type);
  //assign source of code for shader
  glShaderSource(shader, 1, &shaderCode, NULL);
  //compile shader
  glCompileShader(shader);

  GLint compileStatus;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);

  if(compileStatus == GL_FALSE){
    if(Type == GL_VERTEX_SHADER)
      std::cout << "Error :: SHADER :: VERTEX_SHADER_COMPILATION_NOT_SUCCESSFUL" << std::endl;
    else 
      std::cout << "Error :: SHADER :: FRAGMENT_SHADER_COMPILATION_NOT_SUCCESSFUL" << std::endl;
    glDeleteShader(shader);
    return 0;
  }

  return shader;

}

//set default values to variables
void ShaderProgram::SetDefault(){
  VertexShader = 0;
  FragmentShader = 0;

  Program = 0;

}

//function to use the shader program
void ShaderProgram::use(){
  glUseProgram(Program);
}

//function to set current shader program to a null program, ie disable any currently in use program
void ShaderProgram::disable(){
  glUseProgram(0);
}

/*
*   Functions to set uniform values within shader program
*/

void ShaderProgram::setFloat(const std::string &name, float value){
  glUniform1f(glGetUniformLocation(Program, name.c_str()), value);
}
    
void ShaderProgram::setInt(const std::string &name, float value){
  glUniform1i(glGetUniformLocation(Program, name.c_str()), value); 
}

void ShaderProgram::setMat4(const std::string &name, const Mat4 &mat){
  glUniformMatrix4fv(glGetUniformLocation(Program, name.c_str()), 1, GL_FALSE, mat.matrixElements);
}

void ShaderProgram::setVec3(const std::string &name, const Vec3 &vec){
  glUniform3f(glGetUniformLocation(Program, name.c_str()), vec.x, vec.y, vec.z); 
}
