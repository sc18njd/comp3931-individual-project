#include "TestWidget.h"

const unsigned int SHADOW_RESOLUTION_W= 1024, SHADOW_RESOLUTION_H = 1024;
float biasValues[] = {0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.5f, 0.5f, 0.5f, 1.0f};

//function to deal with keyboard inputs
void TestWidget::keyPressed(QKeyEvent *event){
  if (event->isAutoRepeat()){
    event->ignore();
    return;
  }
  switch(event->key()){
    case Qt::Key_W: 
      camera.moveForward();
      break;
    case Qt::Key_A:
      camera.moveLeft();
      break;    
    case Qt::Key_S:
      camera.moveBackward();
      break;
    case Qt::Key_D:
      camera.moveRight();
      break;
    case Qt::Key_R:
      camera.moveUp();
      break;
    case Qt::Key_F:
      camera.moveDown();
      break;
    case Qt::Key_K: //enable viewing of shadow map texture debug
      if(showShadowMap)
        showShadowMap = false;
      else
        showShadowMap = true;
      break;
    case Qt::Key_P: //enable mouse tracking to rotate the camera
      if(mouseTrack)
        mouseTrack = false;
      else
        mouseTrack = true;
      setMouseTracking(mouseTrack);
      break;
    default:
      event->ignore();
      return;
  }
  this->repaint();
}


// constructor
TestWidget::TestWidget(QWidget *parent):
  QGLWidget(parent),
  shadowMap(),
  camera()
{ // constructor


} // constructor

// called when OpenGL context is set up
void TestWidget::initializeGL(){ // initializeGL()
  initializeGLEW = glewInit();

  fps = 0;

  mouseTrack = false;

  glClearColor(0.f, 0.f, 1.f, 1.0f); //set world background colour
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  //load shaders
  shadowMap.Load("ShadowMap.vs","ShadowMap.fs"); 
  
	initDepthMap(); //initialise the depth map texture

  
  lightProjectionMatrix = Mat4::createOrthographicMatrix(-10.0f,10.0f,-10.0f,10.0f,1.0f,7.5f);
  //set values for the separate dialogue box
  _lightingPar.x = -2.0f;
  _lightingPar.y = 4.0f;
  _lightingPar.z = -1.0f;

  //set lighting values
  float temp1[] = {0.25f, .25f, .25f, 1.0f};
  float temp2[] = {0.75f, .75f, .75f, 1.0f};

  glLightfv(GL_LIGHT0, GL_AMBIENT, temp1);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, temp2);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 1.0f / 128.0f);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 1.0f / 256.0f);

  //set view and inverse view matrix pointers
  camera.setMatrixPointer((float*)viewMatrix.matrixElements, (float*)viewMatrixInverse.matrixElements);

  //initialise bias matrix with values
  biasMatrix = Mat4(biasValues);

  showShadowMap = false;

} // initializeGL()


//called when window is resized
void TestWidget::resizeGL(int w, int h){
  glViewport(0,0,w,h);

  //update world projection matrix
  projectionMatrix = Mat4::createPerspectiveMatrix(45.0f, (float)width()/(float)height(),.1f,100.0f);
  projectionMatrixInverse = Mat4::inverseMatrix(projectionMatrix);

  updateGL();
}

//function to initialise the depth map texture
void TestWidget::initDepthMap(){

  //generate shadow map texture and fbo
  glGenFramebuffers(1, &depthMapFBO);
  glGenTextures(1, &depthMap);
  glBindTexture(GL_TEXTURE_2D, depthMap);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_RESOLUTION_W, SHADOW_RESOLUTION_H, 0, GL_DEPTH_COMPONENT,GL_FLOAT,NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  //attach depth texture to frame buffer object
  glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
  glBindTexture(GL_TEXTURE_2D, depthMap);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);

  //check everything working correctly
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
    std::cout << "Warning: FBO not complete!" << std::endl;
  }

  //disable FBO
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

}


//Function to render the shadow map to depth texture
void TestWidget::renderShadowMap(){

  //set viewport to shadow resolution
  glViewport(0, 0, SHADOW_RESOLUTION_W, SHADOW_RESOLUTION_H);

  //initialise world with projection and view matrices
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf((float*)lightProjectionMatrix.matrixElements);

  //set view matrix from lights point of view
  lightViewMatrix = Mat4::createViewMatrix(lightPosition, Vec3(0.0f,0.0f,0.0f), Vec3(0.0f, 1.0f, 0.0f));

  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixf((float*)lightViewMatrix.matrixElements);

  //activate texture
  glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
  glDrawBuffers(0, NULL); 
  glReadBuffer(GL_NONE); //set buffers to only be black or white
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
  
  glClear(GL_DEPTH_BUFFER_BIT);
  		
  glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	glCullFace(GL_FRONT);

  ///////////////////////////////////////////////////////////////////////////////////////////
  //drawing in objects into world
  //plane
  GLfloat normal[3] = {0.,1.,0.};
  glNormal3fv(normal);
  glBegin(GL_POLYGON);
  glVertex3f(-25.0,-0.5,-25.0); 
  glVertex3f(25.0,-0.5,-25.0); 
  glVertex3f(25.0,-0.5,25.0);  
  glVertex3f(-25.0,-0.5,25.0); 
  glEnd();

  //cube
  glNormal3fv(normal);
  glBegin(GL_POLYGON);
  glVertex3f(-1,1,-1);
  glVertex3f(1,1,-1);
  glVertex3f(1,1,1);
  glVertex3f(-1,1,1);
  glEnd();
  glLoadMatrixf(lightViewMatrix.matrixElements);
  glPushMatrix();
  glTranslatef(0.0f,1.5f, 0.0f);
  glutSolidCube(0.5f);
  glPopMatrix();

  glPushMatrix();
  glTranslatef(2.0f,0.0f, 1.0f);
  glutSolidCube(0.5f);
  glPopMatrix();

  glPushMatrix();
  glTranslatef(-1.0f,0.0f, 2.0f);
  glPushMatrix();
  glRotatef(60,1.0f,0.0f,1.0f);;
  glutSolidCube(0.5f);
  glPopMatrix();
  glPopMatrix();

  glCullFace(GL_BACK);

  //teapot
  glLoadMatrixf(lightViewMatrix.matrixElements);
  glPushMatrix();
  glTranslatef(2.0f, 1.0f, -0.5f);
  glutSolidTeapot(0.5f);
  glPopMatrix();

  //disable texture
  glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);

	glBindFramebufferEXT(GL_FRAMEBUFFER, 0);
}


//Fucntion to render the scene using the shadow map depth texture to create shadows
void TestWidget::renderScene(){

  //set projection and view matrix
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glLoadMatrixf((float*)projectionMatrix.matrixElements);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glLoadMatrixf((float*)viewMatrix.matrixElements);

  //set light position in world coordinates
  float temp[] = {lightPosition.x, lightPosition.y, lightPosition.z, 1.0};
  glLightfv(GL_LIGHT0,GL_POSITION,temp);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);

  //use the shader program
  shadowMap.use();
  
  
  shadowMatrix = biasMatrix * lightProjectionMatrix * lightViewMatrix * viewMatrixInverse; //set shadow matrix

  //update uniform values in shader program
  shadowMap.setMat4("ShadowMatrix", shadowMatrix);
  shadowMap.setInt("ShadowMap", 0);

  //activate depth map texture for use
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, depthMap);

  glCullFace(GL_FRONT);
  
  ///////////////////////////////////////////////////////////////////////////////////
  //render objects into scene

  //plane
  glColor3f(1.0f,1.0f,1.0f);
  GLfloat normal[3] = {0.,1.,0.};
  glNormal3fv(normal);
  glBegin(GL_POLYGON);
  glVertex3f(-25.0,-0.5,-25.0); 
  glVertex3f(25.0,-0.5,-25.0); 
  glVertex3f(25.0,-0.5,25.0);  
  glVertex3f(-25.0,-0.5,25.0); 
  glEnd();

  //cube
  glDisable(GL_CULL_FACE);
  glColor3f(.0f,1.0f,.0f);

 
  glNormal3fv(normal);
  glBegin(GL_POLYGON);
  glVertex3f(-1,1,-1);
  glVertex3f(1,1,-1);
  glVertex3f(1,1,1);
  glVertex3f(-1,1,1);
  glEnd();

  glLoadMatrixf((float*)viewMatrix.matrixElements);
  glPushMatrix();
  glTranslatef(0.0f,1.5f, 0.0f);
  glutSolidCube(0.5f);
  glPopMatrix();


  glLoadMatrixf((float*)viewMatrix.matrixElements);
  glPushMatrix();
  glTranslatef(2.0f,0.0f, 1.0f);
  glutSolidCube(0.5f);
  glPopMatrix();


  glLoadMatrixf((float*)viewMatrix.matrixElements);
  glPushMatrix();
  glTranslatef(-1.0f,0.0f, 2.0f);
  glPushMatrix();
  glRotatef(60,1.0f,0.0f,1.0f);;
  glutSolidCube(0.5f);
  glPopMatrix();
  glPopMatrix();

  glColor3f(1.0f,.0f,.0f);
  
  glLoadMatrixf((float*)viewMatrix.matrixElements);
  glPushMatrix();
  glTranslatef(2.0f, 1.0f, -0.5f);
  glutSolidTeapot(0.5f);
  glPopMatrix();
  
  //disable shader program and shadsow map texture
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, 0);

  shadowMap.disable();

  glDisable(GL_DEPTH_TEST);
}


//function to render shadow map depth texture to b&w quadrant fro visual debugging
void TestWidget::renderDepthToQuad(){
  //disable lighting
  glDisable(GL_LIGHTING);

  //set projection matrix
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf((float*)Mat4::createOrthographicMatrix(-1.0f,1.0f,-1.0f,1.0f,-1.0f,1.0f).matrixElements);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  //enable use of texture
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, depthMap);

  //draw texture to quad
  glBegin(GL_QUADS);
    glColor3f(1,1,1);
      glTexCoord2f(0.0f, 0.0f); glVertex2f(-1.0f, -1.0f);
			glTexCoord2f(1.0f, 0.0f); glVertex2f(1.0f, -1.0f);
			glTexCoord2f(1.0f, 1.0f); glVertex2f(1.0f, 1.0f);
			glTexCoord2f(0.0f, 1.0f); glVertex2f(-1.0f, 1.0f);
  glEnd();

  glBindTexture(GL_TEXTURE_2D, 0);		
  glDisable(GL_TEXTURE_2D);
}

// called every time the widget needs painting
void TestWidget::paintGL(){ // paintGL()
  //compute shadow map
  lightPosition = Vec3(_lightingPar.x,_lightingPar.y,_lightingPar.z);
  renderShadowMap();

  glViewport(0, 0, width(), height());
  
  //render scene using shadow map texture
  renderScene();
  
  //display shadow map
  if(showShadowMap)
    renderDepthToQuad();

} // paintGL()


//function to deal with double mouse clicking event, creates separate dialogue UI for updating light position 
void TestWidget::mouseDoubleClickEvent(QMouseEvent * event){
  QDialog *widget = new QDialog;
  _ui.setupUi(widget);
  this->loadDialog(_ui);
  widget->exec();
  this->unloadDialog(_ui);
}

//function to deal with mouse movement, calls function in camera class to update values
void TestWidget::mouseMoveEvent(QMouseEvent * e){
  camera.mouseUpdate(e->x(),e->y());
  updateGL();
}

//function to load separate dialogue box for updating ligh position
void TestWidget::loadDialog(const Ui_Dialog& dialog){
  _ui.posX->setText(QString::number(lightPosition.x));
  _ui.posY->setText(QString::number(lightPosition.y));
  _ui.posZ->setText(QString::number(lightPosition.z));

}

//function to unload and process data input in separate dialogye box
void TestWidget::unloadDialog(const Ui_Dialog& dialog){
  _lightingPar.x = dialog.posX->toPlainText().toFloat();
  _lightingPar.y = dialog.posY->toPlainText().toFloat();
  _lightingPar.z = dialog.posZ->toPlainText().toFloat();
  updateGL();
}

//function to qrite FPS to console
void TestWidget::showFPS(){
  std::cout << fps << "fps"<< std::endl;;
  fps = 0;
}

//function that updates the QWidget
void TestWidget::processing(){
  updateGL();
  fps++;
}