Individual Project - Shadow Mapping

This project aims to create shadows within a virtual world using the shadow mapping alogorithm.
This project should run off a linux machine, namely Feng-linux or DEC-10 machines.

Instructions to compile and run:

1. Navigate to the Test folder.
2. Launch a Terminal from this folder and run the command "qmake" in the command line.
3. Once the Makefile has been created, use the command "make" in the command line.
4. A new application should have compiled, to run use the command "./Test".


Instructions for use:
When using the program, there are a number of user interactions that can be accomplished.

First of all the 'camera' for the world can be moved using the following keyboard inputs:
- 'W' move forward
- 'S' move backwards
- 'A' move left
- 'D' move right
- 'R' move up
- 'F' move down

Other keyboard inputs that can be used are as follows:
- 'P' enable/disable mousetracking for camera rotation
- 'K' enable/disable viewing shadow map visual debug in Quad


The mouse can be used to rotate the camera by moving the mouse across the application screen,
but only when this functionality is enabled, using 'P'.

Finally, by double clicking on the application you can create a separate window that allows the
user to update the position of the light within the world.
